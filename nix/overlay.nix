final: prev: {
  psi4Builder = prev.callPackage ./builder/psi4.nix { psi4 = prev.qchem.psi4; };

  multiwfnBuilder = prev.callPackage ./builder/cubes.nix { multiwfn = prev.qchem.multiwfn; };

  vmdBuilder = prev.callPackage ./builder/render.nix { vmd = prev.qchem.vmd; };

  annotater = prev.haskellPackages.callCabal2nix "Annotater" ../Annotater { };

  annotate = prev.callPackage ./builder/annotate.nix { annotater = final.annotater; };

  compressor = prev.callPackage ./builder/compressor.nix { };

  fullBuilder = prev.callPackage ./builder/fullBuilder.nix {
    psi4Builder = final.psi4Builder;
    multiwfnBuilder = final.multiwfnBuilder;
    vmdBuilder = final.vmdBuilder;
  };
}
