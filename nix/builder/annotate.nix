{ stdenvNoCC, annotater }:

{ name ? ""
, storePath
, orbitalsRange ? 0
} :

stdenvNoCC.mkDerivation rec {
  inherit name;

  phases = [ "runPhase" "installPhase" ];

  nativeBuildInputs = [ annotater ];

  runPhase = ''
    for i in ${storePath}/*.x3d; do
      annotater --file=$i --orbs=${builtins.toString orbitalsRange} > $(basename $i .x3d)_annotated.x3d
    done
  '';

  installPhase = ''
    mkdir $out

    for i in *_annotated.x3d; do
      cp $i $out/.
    done
  '';
}
