{ stdenvNoCC }:

{ name ? ""
, storePath
} :

stdenvNoCC.mkDerivation rec {
  inherit name;

  phases = [ "runPhase" "installPhase" ];

  runPhase = ''
    for i in ${storePath}/*_annotated.x3d; do
      cp $i .
      gzip $(basename $i)
    done
  '';

  installPhase = ''
    mkdir $out

    for i in *_annotated.x3d.gz; do
      cp $i $out/.
    done
  '';
}
