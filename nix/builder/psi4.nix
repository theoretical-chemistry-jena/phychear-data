{ stdenvNoCC, writeTextFile, psi4 }:

{ coords ? ""
, name ? "molecule"
, otherFiles ? [ ]
} :

let
  input = writeTextFile {
    name = "${name}.psi";
    text = ''
      memory 10 GiB

      molecule {
        0 1
        ${coords}
      }

      set {
        basis cc-pvdz
        reference rhf
      }

      optimize("hf")
      o, wfn = frequency("hf", return_wfn = True)
      molden(wfn = wfn, filename = "${name}.molden", dovirtual = True)
    '';
  };

  filesToKeep = [ "${input}" "${name}.out" "${name}.molden" ] ++ otherFiles;

in stdenvNoCC.mkDerivation rec {
  inherit name;

  phases = [ "runPhase" "installPhase" ];

  nativeBuildInputs = [ psi4 ];

  runPhase = "OMP_NUM_THREADS=1 psi4 -i ${input} -o ${name}.out -n 1";

  installPhase = ''
    mkdir $out

    for i in ${toString filesToKeep}; do
      cp $i $out/.
    done
  '';
}
