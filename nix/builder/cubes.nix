{ stdenvNoCC, writeTextFile, multiwfn }:

{ name ? ""
, moldenFile
, orbitalsRange ? 0
} :

let
  range = builtins.genList (x: x) orbitalsRange;
  pList = builtins.map (p: "l+" + builtins.toString p) range;
  nList = builtins.map (p: "h-" + builtins.toString p) range;
  orbInteraction = builtins.foldl' (acc: orb: acc + "3" + "\n" + orb + "\n" + "3" + "\n") "" (pList ++ nList);


  input = writeTextFile {
    name = "multiwfn-orbital-interaction";
    text = "200\n" + orbInteraction + ''
      0
      q
    '';
  };

in stdenvNoCC.mkDerivation rec {
  inherit name;

  phases = [ "runPhase" "installPhase" ];

  nativeBuildInputs = [ multiwfn ];

  runPhase = "Multiwfn ${moldenFile} < ${input}";

  installPhase = ''
    mkdir $out

    for i in *.cub;
      do cp $i $out/${name}_$(basename $i .cub).cube
    done
  '';
}
