{ stdenvNoCC, writeTextFile, vmd }:

{ name
, cubeDir
, moldenFile
, orbitalsRange ? 0
, isoOrb ? "0.02"
} :

let
  range = builtins.genList (x: x) orbitalsRange;
  pList = builtins.map (p: "l+" + builtins.toString p) range;
  nList = builtins.map (p: "h-" + builtins.toString p) range;

  # cubeFiles = builtins.map (n: "${storePath}/${name}_${n}.cube")  (nList ++ pList);
  cubeFiles = builtins.map (n: "${name}_${n}.cube")  (nList ++ pList);

  input = writeTextFile {
    name = "renderX3D.tcl";
    text = ''
      # Set variables for VMD
      set IsoOrb ${isoOrb}

      # Display settings
      axes location off
      color Element C gray
      color Element Cl green
      mol color element

      # Plot the atoms as ball and stick model
      mol load molden ${moldenFile}
      mol delrep 0 0
      mol selection all
      mol material AOChalky
      mol representation CPK 1.0 0.3 50.0 50.0
      mol color Element
      mol addrep 0

      # Detect point group
      set sel [atomselect top all]
      set result [measure symmetry $sel]
      array set symm $result
      puts $symm(pointgroup)
      puts $symm(order)
      puts $symm(elements)
      puts $symm(axes)

      set Iter 0

      # Plot all orbitals. They will overlap but can be labeled in the X3D file later.
      foreach Orb [list ${builtins.toString cubeFiles}] {
        # Load cubes as volumetric information into the existing modesl
        mol addfile ${cubeDir}/$Orb type {cube} volsets {0} 0

        # draw positive and negative isosurface for the orbitals
        mol material Glass2
        mol color ColorID 0
        mol representation Isosurface $IsoOrb $Iter 0 0 1 1
        mol addrep 0

        mol material Glass2
        mol color ColorID 3
        mol representation Isosurface [expr -1 * $IsoOrb] $Iter 0 0 1 1
        mol addrep 0

        incr Iter
      }

      # After all data have been loaded, save as X3D file
      render X3D ${name}.x3d

      quit
    '';
  };

in stdenvNoCC.mkDerivation rec {
  inherit name;

  phases = [ "runPhase" "installPhase" ];

  nativeBuildInputs = [ vmd ];

  runPhase = ''
    vmd -eofexit -dispdev none < ${input}
  '';

  installPhase = ''
    mkdir $out

    cp ${name}.x3d $out/.
  '';
}
