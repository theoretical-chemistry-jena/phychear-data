{ symlinkJoin, writeTextFile, psi4Builder, multiwfnBuilder, vmdBuilder, annotate, compressor }:

{ coords, name, orbitalsRange ? 2 }:

let
  # Run quantum chemistry wavefunction calculation and optimisation.
  psi4Calc = psi4Builder { inherit coords name; };
  moldenFile = "${psi4Calc}/${name}.molden";

  # Calculate orbital cube files with Multiwfn around HOMO-LUMO gap.
  cubeCalc = multiwfnBuilder { inherit name orbitalsRange moldenFile; };

  x3dCalc = vmdBuilder {
    inherit name orbitalsRange moldenFile;
    cubeDir = cubeCalc;
  };

  annotation = annotate {
    inherit name orbitalsRange;
    storePath = x3dCalc;
  };

  compress = compressor {
    inherit name;
    storePath = annotation;
  };

in symlinkJoin {
  inherit name;
  paths = [
    psi4Calc
    cubeCalc
    x3dCalc
    annotation
    compress
  ];
}
