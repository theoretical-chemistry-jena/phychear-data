# PhycheAR-Data
[![built with nix](https://builtwithnix.org/badge.svg)](https://builtwithnix.org)

Infrastructure, data, build scripts, and toolings for the PhycheAR/OrbitalViewer app.

## App
The Android app can be installed from the [Google Play store](https://play.google.com/store/apps/details?id=com.InfAI.PhyChe).

The iOS app can be installed from the [App Store](https://apps.apple.com/om/app/phychear/id1560335936).

## Usage
The AR app reads annotated X3D files, that can either be on the internal mobile phone storage, or be distributed from a plain http server.
These annotated X3D files are created by a series of steps:

  1. Perform a geometry optimisation with Psi4, and calculate the wavefunction. The calculation produces a Molden file.
  2. Multiwfn generates individual orbital cubes from the Molden file.
  3. The molecular geometry (from the Molden files) and orbitals are rendered into a single representation using VMD Tcl scripts. VMD produces X3D files with 3D meshes and material properties for all objects.
  4. A Haskell program annotates the X3D files with knowledge of how VMD structures X3D files. Each object, that can be assigned will get a `datatype` XML attribute in the X3D `<Scene>` description.
  This allows the app identifying individual orbitals, the molecule and potentially other substructures.
  5. GZip compresses the files. This reduces the file size significantly and can natively be decompressed by the App.

All of the above steps are conveniently automated and fully reproducible by formulating this workflow in Nix.

From within the top level directory of this repository simply executing
```bash
  nix-build
```
will create all files, starting from the quantum chemistry calculations (of course this requires a working Nix installation).
The results for each for each molecule will be in the `result*/` directories and can be directly copied to the mobile phone.

The final files can either be downloaded as artifacts from the latest pipeline or built locally.

## Hosting, CI/CD
The repository utilises the special GitLab runners, that are registered for the GitLab group, to automatically build and efficiently cache everything with Nix.
Artifacts for recent commits are available from the CI.
Data from tags in the `main` branch are also automatically deployed to `https://troja.ipc3.uni-jena.de/phyche/` and `http://physchemar.uni-jena.de/phyche` and can be distributed from there.
Also see the [Environments of this repository](https://gitlab.com/theoretical-chemistry-jena/phychear-data/-/environments) for deploys.

### Adding Molecules
The chemical problems are formulated in the `MoldData` directory.
Each of its subdirectories contains the workflow for one molecule.
To add another molecule, just create a new directory below `MoldData` and copy one of the other examples.
It is enough to change `name`, and `coords` (cartesian in Angstroms) to formulate another closed shell neutral molecule.
The new file (e.g. `MoldData/test/default.nix`) now needs to be called from `MolData/default.nix`.
Therefore add a line like
```nix
  test = callPackage ./test { };
```
to `MolData/default.nix` and execute `nix-build` from within the top level directory again.
