{
  description = "Data Generation for the PhycheAR app";

  inputs = {
    nixpkgs.url = "nixpkgs/nixpkgs-unstable";

    qchem.url = "github:markuskowa/nixos-qchem/master";

    flake-utils.url = "github:numtide/flake-utils";

    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, qchem, flake-utils, ... }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          config = {
            allowUnfree = true;
            qchem-config = {
              optAVX = false;
              allowEnv = false;
            };
          };
          overlays = [ qchem.overlay (import ./nix/overlay.nix) ];
        };

        molData = import ./MolData/default.nix { inherit pkgs; };
      in {

        packages = {
          default = pkgs.symlinkJoin {
            name = "data";
            paths = with molData; [
            azan buta-1_3-diene dichlorethen oxidan E-1_2-dichlorethen
            Z-1_2-dichlorethen
            ];
          };
          inherit (molData)
            azan buta-1_3-diene dichlorethen oxidan E-1_2-dichlorethen
            Z-1_2-dichlorethen;
        };

        defaultPackage = self.packages."${system}".default;

        legacyPackages = pkgs;

        devShells.default = let
          hsPkgs = pkgs.haskellPackages.ghcWithPackages
            (p: with p; [ rio xml-conduit attoparsec text cmdargs ]);
        in pkgs.mkShell {
          buildInputs = with pkgs; [
            pkgs.qchem.psi4
            pkgs.qchem.multiwfn
            pkgs.qchem.vmd
            avogadro2
            tachyon

            hsPkgs
            haskell-language-server
            ormolu
            cabal-install
            hpack
          ];
        };

        devShell = self.devShells."${system}".default;
      });
}
