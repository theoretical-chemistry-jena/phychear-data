{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE NoImplicitPrelude #-}

-- |
-- Module      : Annotater.X3D
-- Description : Annotations of X3D files
-- Copyright   : Phillip Seeber, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
module Annotater.X3D (transformX3D, AnnoArgs (..), HasFile (..), annoArgs) where

import Data.Attoparsec.Text
import qualified Data.Map.Strict as Map
import RIO hiding (Builder)
import RIO.State
import System.Console.CmdArgs as CmdArgs
import Text.XML

-- | Configuration options.
data AnnoArgs = AnnoArgs
  { orbs :: Int,
    file :: FilePath
  }
  deriving (Show, Data, Typeable)

class HasOrbs a where
  orbsL :: Lens' a Int

class HasFile a where
  fileL :: Lens' a FilePath

instance HasOrbs AnnoArgs where
  orbsL = lens orbs $ \s b -> s {orbs = b}

instance HasFile AnnoArgs where
  fileL = lens file $ \s b -> s {file = b}

annoArgs :: AnnoArgs
annoArgs =
  AnnoArgs
    { orbs = 1 &= typ "INT" &= help "Number of orbitals around the HOMO-LUMO gap",
      file = CmdArgs.def &= typFile &= help "Path to an X3D file"
    }

----------------------------------------------------------------------------------------------------

-- | Tracker for data are currently in scope.
data Status
  = -- | Unclear which data are being processed
    Unknown
  | -- | Atoms and bonds
    Molecule
  | -- | Orbitals
    Orbital Orbital
  deriving (Eq, Show)

-- | Tracker for orbitals
data Orbital
  = -- | HOMO - \(n\)
    HOMO Natural WFSign
  | -- | LUMO + \(n\)
    LUMO Natural WFSign
  deriving (Eq, Show)

-- | Sign of the wavefunction
data WFSign = Positive | Negative deriving (Eq, Show)

----------------------------------------------------------------------------------------------------

-- | State of the scene processing.
data SceneState = SceneState
  { dataType :: Status,
    procNodes :: [Node]
  }

----------------------------------------------------------------------------------------------------

-- | Annotates an X3D file with very strict assumptions.
transformX3D :: (HasOrbs env, MonadReader env m) => Document -> m Document
transformX3D doc = annotate doc

-- | Works on the @<Scene>@ hierarchy of the X3D file and processes all its nodes. VMD comments are
-- used to track the data.
annotate :: (HasOrbs env, MonadReader env m) => Document -> m Document
annotate doc@Document {documentRoot} = case documentRoot of
  -- The body of an X3D document.
  rootEl@Element {elementName = Name {nameLocalName = "X3D"}, elementNodes} -> do
    transX3DNodes <- traverse matchApplyScene elementNodes
    return $ doc {documentRoot = rootEl {elementNodes = transX3DNodes}}
  -- Must be another XML document, but is not X3D. Return unchanged.
  _ -> return doc
  where
    -- Looks for the @<Scene>@ node and applies a function to all its elements.
    matchApplyScene :: (HasOrbs env, MonadReader env m) => Node -> m Node
    matchApplyScene node = case node of
      NodeElement sceneEl@Element {elementName = Name {nameLocalName = "Scene"}, elementNodes} -> do
        finalStat <- flip execStateT initSceneState . traverse procSceneNode $ elementNodes
        return . NodeElement $ sceneEl {elementNodes = procNodes finalStat}
      n -> return n

    -- Statefull processing of a node in the @<Scene>@
    procSceneNode :: (HasOrbs env, MonadReader env m, MonadState SceneState m) => Node -> m ()
    procSceneNode sceneNode = case sceneNode of
      -- Add @DEF@ attribute to a @<Transform>@
      NodeElement transEl@Element {elementName = Name {nameLocalName = "Transform"}, elementAttributes} -> modify $
        \currStat@SceneState {dataType, procNodes} ->
          let newTransEl = transEl {elementAttributes = elementAttributes <> stat2DefAttr dataType}
              newNode = NodeElement newTransEl
           in currStat {procNodes = procNodes ++ [newNode]}
      -- Wrap a @<Shape>@ in a @<Transform>@ and add a @DEF@ attribute to the @<Transform>@.
      NodeElement shapeEl@Element {elementName = Name {nameLocalName = "Shape"}} -> modify $
        \currStat@SceneState {dataType, procNodes} ->
          let wrappedShapeEl =
                Element
                  { elementName = transformName,
                    elementAttributes = stat2DefAttr dataType,
                    elementNodes = pure $ NodeElement shapeEl
                  }
              newNode = NodeElement wrappedShapeEl
           in currStat {procNodes = procNodes ++ pure newNode}
      -- Parse a comment to obtain information about the data types of the following nodes.
      NodeComment comment -> do
        nOrbGap <- view orbsL
        case parseOnly (vmdComment $ fromIntegral nOrbGap) comment of
          Left _ -> modify $ \s@SceneState {procNodes} ->
            s {dataType = Unknown, procNodes = procNodes ++ [NodeComment comment]}
          Right newData -> modify $ \s@SceneState {procNodes} ->
            s {dataType = newData, procNodes = procNodes ++ [NodeComment comment]}
      -- Node content and node instructions are used unchanged.
      otherNode -> modify $ \s@SceneState {procNodes} -> s {procNodes = procNodes ++ [otherNode]}

    -- Initial scene state
    initSceneState = SceneState {dataType = Unknown, procNodes = mempty}

    -- The name-scope of the @<Scene>@
    transformName = Name {nameLocalName = "Transform", nameNamespace = Nothing, namePrefix = Nothing}
    defAttr = Name {nameLocalName = "DEF", nameNamespace = Nothing, namePrefix = Nothing}

    -- Make a @DEF@ attribute from a data type 'Status'
    stat2DefAttr :: Status -> Map Name Text
    stat2DefAttr stat = Map.singleton defAttr $ case stat of
      Molecule -> "Molecule"
      Orbital (HOMO n _) -> "HOMO - " <> tshow n
      Orbital (LUMO n _) -> "LUMO + " <> tshow n
      Unknown -> "Unknown"

-- | Parser for XML/X3D comment strings as produced by VMD, indicating which data follows.
vmdComment ::
  -- | Number of additional orbitals around the HOMO-LUMO gap.
  Natural ->
  Parser Status
vmdComment orbRange = do
  skipMany (char ' ')
  void $ string "MoleculeID: " *> decimal @Natural
  skipMany (char ' ')
  reprID <- string "ReprID: " *> (decimal @Natural)
  void takeText
  case reprID of
    0 -> return Molecule
    n -> return . Orbital . getOrb $ n
  where
    orbCounter n = ((n - 1) `div` 2) + 1
    nOrbs = orbRange * 2
    homo = nOrbs `div` 2
    getOrb n =
      if orbCounter n <= homo
        then HOMO (homo - orbCounter n) (if even n then Negative else Positive)
        else LUMO (orbCounter n - homo - 1) (if even n then Negative else Positive)
