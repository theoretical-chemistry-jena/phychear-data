-- |
-- Module      : Annotater.X3D
-- Description : Annotations of X3D files
-- Copyright   : Phillip Seeber, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
module Main (main) where

import Annotater.X3D
import Data.Text.Lazy.IO (hPutStrLn)
import RIO
import System.Console.CmdArgs as CmdArgs
import Text.XML as XML (def, readFile, renderText)

data ReadException = ReadException deriving (Show)

instance Exception ReadException

main :: IO ()
main = runSimpleApp $ do
  logDebug "Annotater for X3D files from VMD"
  options <- liftIO . cmdArgs $ annoArgs
  orig <- liftIO . XML.readFile XML.def $ options ^. fileL
  let transformed = flip runReader options . transformX3D $ orig
  liftIO . hPutStrLn stdout . XML.renderText XML.def $ transformed
  logDebug "Done"
