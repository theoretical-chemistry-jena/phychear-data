{ fullBuilder } :

let
  name = "E-1_2-dichlorethen";

  coords = ''
    C       -4.30882        1.80470        0.03248
    C       -4.36351        0.47395       -0.02920
    Cl      -4.93538        2.78289       -1.28587
    H       -3.87745        2.30115        0.89602
    H       -4.79671       -0.01085       -0.89693
    Cl      -3.73985       -0.51230        1.28351
  '';

in fullBuilder { inherit name coords; }
