{ fullBuilder } :

let
  name = "oxidan";

  coords = ''
    O       -1.94257       -1.67220       -1.15756
    H       -0.97257       -1.67220       -1.15756
    H       -2.26590       -2.40031       -0.60420
  '';

in fullBuilder { inherit name coords; }
