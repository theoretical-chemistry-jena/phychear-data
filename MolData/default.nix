{ pkgs ? import ../nix/pkgs.nix }:

with pkgs; rec {
  azan = callPackage ./azan { };

  buta-1_3-diene = callPackage ./buta-1_3-diene { };

  dichlorethen = callPackage ./1_1-dichlorethen { };

  oxidan = callPackage ./oxidan { };

  E-1_2-dichlorethen = callPackage ./E-1_2-dichlorethen { };

  Z-1_2-dichlorethen = callPackage ./Z-1_2-dichlorethen { };
}
