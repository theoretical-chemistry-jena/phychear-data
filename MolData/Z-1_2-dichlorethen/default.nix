{ fullBuilder } :

let
  name = "Z-1_2-dichlorethen";

  coords = ''
    C       -2.95792        0.31418       -1.84122
    C       -3.00584       -1.02108       -1.88477
    Cl      -2.23528        1.18347       -0.49223
    H       -3.37536        0.89303       -2.65931
    Cl      -2.35045       -2.02583       -0.59690
    H       -3.46174       -1.51408       -2.73782
  '';

in fullBuilder { inherit name coords; }
