{ fullBuilder } :

let
  name = "1_1-dichloroethen";

  coords = ''
    C       -6.65188        2.61329       -0.02554
    C       -6.62226        1.28045       -0.03603
    Cl      -5.24128        3.53164       -0.52535
    Cl      -8.09778        3.46867        0.48678
    H       -5.72765        0.75431       -0.35301
    H       -7.48898        0.70369        0.27106
  '';

in fullBuilder { inherit name coords; }
