{ fullBuilder } :

let
  name = "azan";

  coords = ''
    N       -2.13464       -0.00542       -1.25291
    H       -1.09153        0.01397       -1.24615
    H       -2.47266        0.75352       -1.88398
    H       -2.47267        0.19657       -0.28677
  '';

in fullBuilder { inherit name coords; }
